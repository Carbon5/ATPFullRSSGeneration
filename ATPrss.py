import time
import re
import sys
from datetime import datetime
import requests as reqs
from lxml import etree

# there is a max requests per time frame on the server
# this function lets us get around it without waiting ages
def DL(page):
    out = None
    while True:
        out = reqs.get(page)
        if out.status_code == 200:
            break
        print('Waiting on page: ', page)
        time.sleep(5)
    return out

# make the xml tree --------------------------------- the 39 is to avoid the encoding data, lxml don't like it
rssTree = etree.XML(DL('http://atp.fm/episodes?format=rss').text[39:])

# get the earliest episode in the feed to avoid duplication
nextPage = '' # typeless initializations are annoying
oldItems = rssTree.xpath('//channel/item')
for desc in rssTree.xpath('//channel/item/description'):
    desc.text = etree.CDATA(desc.text)
curPage = rssTree.xpath('//channel/item/link')[-1].text
pageTree = etree.HTML(DL(curPage).text)
for node in pageTree.xpath('//a'):
    if node.text == 'Older':
        nextPage = 'http://atp.fm' + node.get('href')
        break

while True:
    # parse the page into a tree
    pageTree = etree.HTML(DL(curPage).text)

    # create the item node that will be inserted into the xml document
    itemTree = etree.SubElement(rssTree.xpath('//channel')[0], 'item')

    # find the audio embed. It has Some of the info needed.
    audio = pageTree.xpath('//div[@class="sqs-audio-embed"]')[0]

    # title
    elem = etree.SubElement(itemTree, 'title')
    elem.text = audio.get('data-title')

    # creator
    creator = etree.SubElement(itemTree, '{'+rssTree.nsmap.get('dc')+'}creator')
    creator.text = pageTree.xpath('//footer//a[@rel="author"]/text()')[0]

    # pubDate: This is the most accurate that I can get off the page.
    pubDate = etree.SubElement(itemTree, 'pubDate')
    dateStr = pageTree.xpath('//header//time')[0].get('datetime')
    dateObj = datetime.strptime(dateStr, '%Y-%m-%d')
    pubDate.text = dateObj.strftime('%a, %d %b %Y %H:%M:%S +0000')

    # link
    link = etree.SubElement(itemTree, 'link')
    link.text = curPage

    # guid
    guid = etree.SubElement(itemTree, 'guid', isPermaLink="false")
    guidStr = pageTree.xpath('//span[@class="squarespace-social-buttons inline-style"]')[0].get('data-asset-url')
    parse = re.search('(?<=static)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)', guidStr)
    guid.text = parse.group(1) + ':' + parse.group(2) + ':' + parse.group(3)

    # description
    description = etree.SubElement(itemTree, 'description')
    noteLst = pageTree.xpath('//div[@class="sqs-block markdown-block sqs-block-markdown" or @class="sqs-block html-block sqs-block-html"]/div/*')
    noteStr = ''
    for note in noteLst:
        noteStr += etree.tostring(note, encoding=str, pretty_print=True)
    if noteStr == '':
        print('Something is wrong on page ', curPage)
        exit()
    description.text = etree.CDATA(noteStr)

    # Iauthor
    Iauthor = etree.SubElement(itemTree, '{'+rssTree.nsmap.get('itunes')+'}author')
    Iauthor.text = audio.get('data-author')

    # Isubtitle - Hard coding this due to lack of info on page
    Isubtitle = etree.SubElement(itemTree, '{' + rssTree.nsmap.get('itunes') + '}subtitle')
    Isubtitle.text = 'Accidental Tech Podcast'

    # Iexpicit
    Iexpicit = etree.SubElement(itemTree, '{' + rssTree.nsmap.get('itunes') + '}explicit')
    Iexpicit.text = 'no'

    # Iduration - Hard coding this due to lack of info on page
    Iduration = etree.SubElement(itemTree, '{' + rssTree.nsmap.get('itunes') + '}duration')
    Iduration.text = '00:00:00'

    # Iimage - Hard coding this due to lack of info on page
    Iimage = etree.SubElement(itemTree, '{' + rssTree.nsmap.get('itunes') + '}image',
                              href='http://static1.squarespace.com/static/513abd71e4b0fe58c655c105/t/52c45a37e4b0a77a5034aa84/1388599866232/1500w/Artwork.jpg')

    # enclosure: The length attribute is empty in audio. Hopefully this doesn't break everything
    enclosure = etree.SubElement(itemTree, 'enclosure', length=audio.get('data-duration-in-ms'),
                                 type=audio.get('data-mime-type'), url=audio.get('data-url'))

    # parse the page and get the link to the previous episode
    for node in pageTree.xpath('//a'):
        if node.text == 'Older':
            nextPage = 'http://atp.fm' + node.get('href')
            break
    else:
        break # if there isn't a link to follow then we are done parsing the website and can finish up

    curPage = nextPage
    time.sleep(1)
    print('Starting: ', curPage)

output = etree.tostring(rssTree, encoding=str, pretty_print=True)

print(output)

# Output to a file instead of to the terminal
file = open('output.xml', 'wb')
file.write(output.encode(sys.stdout.encoding, errors='xmlcharrefreplace'))
file.close()
