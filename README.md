# Readme

### Purpose

The Accidental Tech Podcast only keeps 100 episodes in their RSS feed. This script parses ATP.fm for all the episodes' information and appends the missing entries to the end of the XML file provided by the RSS feed at http://atp.fm/episodes?format=rss. This results in a complete XML file with all the episodes having as much information as could be found for them.

### Dependencies

This script runs with Python 3 and uses the following modules:
* datetime
* lxml
* re (regular expressions)
* requests
* sys (system)
* time

### Usage

Follow these steps:

1. Make sure the requisite modules are installed
2. Run the script to produce the final XML file
3. Modify the published date in that file to be newer then what is currently


Now you can point your podcast app of choice at this file to populate the old episodes into the list. Online hosting for the file can be done easily by using Dropbox, Google Drive, or similar and creating a share link for the app to access it.

My podcast app keeps old episodes in its internal list even after they have been removed from the RSS feed, so I only had to provide this expanded list once. I may come back to this at some point and add another script that merges the official feed with this expanded list for an ongoing creation of the full list.

### Known Bugs

There is a "Powered by Squarespace" bit added at the end of the show notes in most generated entries. This is due to a difference in how the webpages were formatted before episode 60.